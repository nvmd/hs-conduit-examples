{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Data.Conduit
import Blaze.ByteString.Builder
import qualified Data.ByteString.Lazy as LBS
import Data.Aeson
import Control.Monad
import Control.Monad.Trans
import Control.Concurrent (threadDelay)

main = scotty 8080 $ do
  get "/api/v1.0/streams/simple"  getSimpleStream
  get "/api/v1.0/streams/counter" getCounterStream

getSimpleStream = do
  header "Content-type" "application/json"
  source simpleSource

getCounterStream = do
  header "Content-type" "application/x-json-stream"
  source counterSource

simpleSource :: Source (ResourceT IO) (Flush Builder)
simpleSource = do
  sendJson [ "message" .= ("Hello!" :: String) ]

counterSource :: Source (ResourceT IO) (Flush Builder)
counterSource = do
  let num = 100 :: Int
  forM_ [1..num] $ \ctr -> do
                     liftIO $ threadDelay 100000
                     sendJson [ "counter" .= ctr ]

sendJson x = do
  sendLBS $ (encode $ object x) `LBS.append` "\n"
  yield Flush
  where
    sendLBS = yield . Chunk . fromLazyByteString
