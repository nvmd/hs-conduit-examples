import Network.HTTP.Conduit
import Data.Conduit
import Data.Conduit.Binary

main = do
  manager <- newManager def
  req <- parseUrl "http://localhost:8080/api/v1.0/streams/simple"
  runResourceT $ do
    res <- http req manager
    responseBody res $$+- sinkFile "simple.json"
