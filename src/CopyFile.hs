import Data.Conduit
import Data.Conduit.Binary

main :: IO ()
main = do
  let source = sourceFile "./src/CopyFile.hs"
      sink   = sinkFile   "/tmp/copy.hs"
  runResourceT $ source $$ sink
