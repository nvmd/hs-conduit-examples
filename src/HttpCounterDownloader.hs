{-# LANGUAGE OverloadedStrings #-}
import Data.Conduit
import Network.HTTP.Conduit
import qualified Data.Conduit.Binary as CB
import qualified Data.ByteString.Char8 as BS
import Control.Monad.Trans

main = do
  manager <- newManager def
  req <- parseUrl "http://localhost:8080/api/v1.0/streams/counter"
  let headers = requestHeaders req
      req' = req {
          requestHeaders = ("Accept", "application/x-json-stream") :
                           headers
        }
  runResourceT $ do
    res <- http req' manager
    responseBody res $$+- CB.lines =$ counterSink

counterSink :: Sink BS.ByteString (ResourceT IO) ()
counterSink = do
  md <- await
  case md of
    Nothing -> return ()
    Just d -> do
      liftIO $ BS.putStrLn "--------"
      liftIO $ BS.putStrLn d
      counterSink
